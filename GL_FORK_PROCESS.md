# Procedures

## Version format

Here's an example of a version for the artifacts deployed from this project:

```
1.69.1-0.0.1-dev-20220722150450
```

- `1.69.1` is the base VSCode version
- `0.0.1` is the version of this fork
- `dev` is a flag that the release is meant only for development purposes.
  This could be replaced with `rc` if the artifact was a release candidate.
- `20220722150450` is the timestamp of the artifacts creation

We'll use GitLab releases to host these release artifacts and create tags based
on the version name of the artifact created in the `main` branch.

**Please note:** Official releases will not have `rc`, `dev`, or timestamp extension.

## Creating release

Only maintainers can create a release (developers can't push tags).

1. Create tag locally using the `./scripts/gl/gl_create_tag.sh <dev|prod>`.

   - Use `dev` argument if you are making dev release.
   - Use `prod` argument if you are making production release.

2. Push the tag (e.g. `git push origin 1.71.1-1.0.0-dev-20221003100000`).
3. When the pipeline succeeds:
   1. Find the release for your version on the [releases page](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/releases).
   2. Find the package version on the [package registry page](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/packages).
   3. Edit the release description and add the link to the released package version.

## Updating base vscode version

### Goals

- Reproducible historic builds
  - Builds need to come from "tags" and release process includes tagging
- Intent of changes
  - Each individual commit will represent a cohesive, atomic change to the
    upstream behavior.
  - History of commits is really important!!
  - Let's include a CHANGELOG for every MR
- Reduce the cognitive load of knowing what changes we are making in our
  fork. This goal is _intentionally_ prioritized over preserving
  the historic state of commits on the fork, which is why we are
  preferring the `rebase ... -onto ...` paradigm over a merge-based
  paradigm. In other words, this fork is treated just like a feature
  branch. Don't fear the `rebase`.
- See some of the arguments made here for this philosophy:
  https://github.com/thewoolleyman/gitrflow/blob/master/readme/goals_and_philosophy.md
  - TL;DR previous versions of history don't matter, a clean current state
    with atomic commits showing cohesive intent is most important. Just like
    any other feature branch.

Downsides of this approach:

- Other than inspecting the history of previous tags, there's
  no way to see the previous versions of history by looking
  at the history of the main branch. But as described above,
  this is an intentional tradeoff to lower the cognitive load
  of understanding individual atomic changes.

### Process

```sh
# Backup current main
git co main
git pull
git push origin main:bkp-main-$(date +%Y-%m-%d)

# Add the upstream remote
git remote add upstream https://github.com/microsoft/vscode.git
git fetch upstream

# Set vars for readability
export PREVIOUS_TAG=1.69.1
export NEW_TAG=1.71.1

# Push the upstream tag to a branch in the gitlab fork repo - will be used as source branch for MR
git co -b upstream-${NEW_TAG} ${NEW_TAG}
git push -u

# Create a new branch for the rebased changes
git co main
git co -b update-fork-to-${NEW_TAG}

# Rebase the fork's changes onto the new upstream tag
git rebase -i ${PREVIOUS_TAG} --onto ${NEW_TAG}
# (resolve any conflicts as part of rebase)

# Push the update-fork branch and create an MR
git push -u
# (create MR in gitlab, changing TARGET branch to upstream-${NEW_TAG}, and TITLE to "Update fork to ${NEW_TAG}")

# Diff the diffs using `git range-diff`
git range-diff ${PREVIOUS_TAG}..main ${NEW_TAG}..update-fork-to-${NEW_TAG}

# Test and Verify everything

# Force push the update-fork branch to the fork's main branch
git push --force-with-lease origin update-fork-to-${NEW_TAG}:main
```

### Notes on Merge Requests

- The process for updating this repo against a new upstream tag involves rewriting/rebasing
  the history of main.
- This means that any MR which is open while the main branch is rebased will need
  to be rebased onto the new upstream.
- This will be a command like:

  ```
  # ensure `main` branch is pulled with latest
  git rebase (previous `git merge-base` SHA) --onto main
  ```
