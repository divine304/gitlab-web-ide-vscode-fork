#!/bin/sh

# This script starts the VS Code server and the SSH server if present
#
# It uses the following environment variables
# $IGNORE_VERSION_MISMATCH - if set to true, the server works even when server and WebIDE versions don't match
# $EDITOR_PORT - port on which the editor is exposed
# $EDITOR_VOLUME_DIR - directory where the editor volume is mounted. This editor
#                      volume contains files copied by the gl_copy_server.sh script

sshd_path=$(which sshd)

if [ -z "${SSH_PORT}" ]; then
  SSH_PORT="60022"
fi

if [ -x "$sshd_path" ]; then
  echo "Starting sshd"
  $sshd_path -D -p $SSH_PORT &
fi

LOG_LEVEL=info

if [ "$IGNORE_VERSION_MISMATCH" = true ]; then
  # TODO: remove this section once issue is fixed - https://gitlab.com/gitlab-org/gitlab/-/issues/373669
  # remove "commit" key from product.json to avoid client-server mismatch
  # TODO: remove this once we are not worried about version mismatch
  # https://gitlab.com/gitlab-org/gitlab/-/issues/373669
  echo "Ignoring VS Code client-server version mismatch"
  sed -i '/"commit"/d' "${EDITOR_VOLUME_DIR}/code-server/product.json"
fi

"${EDITOR_VOLUME_DIR}/code-server/bin/gitlab-webide-server" --host "0.0.0.0" --port "${EDITOR_PORT}" --without-connection-token --log "${LOG_LEVEL}"

